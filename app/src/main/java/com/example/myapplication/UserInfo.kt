package com.example.daskvniti_gamocda

data class UserInfo(
    val name: String = "",
    val image: String = "",
    val address: String = "",
    val mailInfo: String = "",
    val phone: String = ""
)
