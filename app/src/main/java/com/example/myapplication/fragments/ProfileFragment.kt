package com.example.myapplication.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.daskvniti_gamocda.UserInfo
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfileFragment : Fragment(R.layout.profile_fragment) {
    private lateinit var saveBtn: Button
    private lateinit var photo: ImageView
    private lateinit var url: EditText
    private lateinit var address: EditText
    private lateinit var mailInfo: EditText
    private lateinit var phone: EditText
    private lateinit var fullName: EditText
    private val auth = FirebaseAuth.getInstance()
    private val dataBase = FirebaseDatabase.getInstance().getReference("userInfo")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        listener()

        dataBase.child(auth.currentUser?.uid!!).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val userInfo:UserInfo = snapshot.getValue(UserInfo::class.java) ?: return
                fullName.setText(userInfo.name)
                address.setText(userInfo.address)
                mailInfo.setText(userInfo.mailInfo)
                phone.setText(userInfo.phone)
                url.setText(userInfo.image)
                Glide.with(this@ProfileFragment).load(userInfo.image).into(photo)
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }

    private fun init() {
        saveBtn = view?.findViewById(R.id.saveBtn)!!
        photo = view?.findViewById(R.id.photo)!!
        url = view?.findViewById(R.id.url)!!
        address = view?.findViewById(R.id.address)!!
        mailInfo = view?.findViewById(R.id.mailInfo)!!
        phone = view?.findViewById(R.id.phone)!!
        fullName = view?.findViewById(R.id.fullName)!!

    }

    private fun listener() {

        saveBtn.setOnClickListener {
            val image = url.text.toString()
            val name = fullName.text.toString()
            val address = address.text.toString()
            val mailInfo = mailInfo.text.toString()
            val phone = phone.text.toString()
            val userInfo = UserInfo(name, image, address, mailInfo, phone)

            dataBase.child(auth.currentUser?.uid!!).setValue(userInfo)

        }
    }



}