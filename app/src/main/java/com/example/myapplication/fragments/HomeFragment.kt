package com.example.myapplication.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class HomeFragment:Fragment(R.layout.home_fragment) {
    private lateinit var addNoteBtn:Button
    private lateinit var notes:List<Note>
    private lateinit var recyclerview:RecyclerView
    private val db = FirebaseDatabase.getInstance().getReference("/notes")


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        notes = listOf( Note("hello","this is my story"),
            Note("hey bro","how are you"),
            Note("hey bro","how are you"),
            Note("hey bro","how are you"),
            Note("hey bro","how are you"),
            Note("hey bro","how are you"),
            Note("hey bro","how are you"),
            Note("hey bro","how are you"),
            Note("hey bro","how are you"),
            Note("hey bro","how are you"),
            Note("hey bro","how are you"),
            Note("hey bro","how are you")


            )

        getNotes()



        addNoteBtn = view.findViewById(R.id.addNoteBtn)
        recyclerview = view.findViewById(R.id.recyclerview)
        recyclerview.layoutManager=LinearLayoutManager(activity)
        recyclerview.adapter = MyRecyclerViewAdapter(notes)




        addNoteBtn.setOnClickListener{

            Navigation.findNavController(requireView())
                .navigate(HomeFragmentDirections.actionHomeFragmentToNewNoteFragment())
        }

    }
    private fun getNotes(){
        db.child(FirebaseAuth.getInstance().uid.toString())
            .addValueEventListener(
                object : ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val notes: Note? = snapshot.getValue(Note::class.java)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }
                }
            )
    }
}