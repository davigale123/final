package com.example.myapplication.fragments

data class Note(
    var title:String,
    var text:String
)