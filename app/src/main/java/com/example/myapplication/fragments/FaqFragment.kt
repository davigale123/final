package com.example.myapplication.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.myapplication.R

class FaqFragment:Fragment(R.layout.faq_fragment){
    private lateinit var button1: Button
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.faq_fragment, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        registerListeners()
    }
    private fun init(){
        button1 = view?.findViewById(R.id.button1)!!
    }
    private fun registerListeners(){
        button1.setOnClickListener {
            Navigation.findNavController(requireView()).navigate(FaqFragmentDirections.actionFaqFragmentToAuthorisationFragment())
        }
    }
}