package com.example.myapplication.fragments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R

class MyRecyclerViewAdapter(val notes:List<Note>) :RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder>(){
    inner class MyViewHolder(myview:View) : RecyclerView.ViewHolder(myview){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val oncreateview = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_layout,parent,false)
        return MyViewHolder(oncreateview)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mynote = notes[position]
        val title = holder.itemView.findViewById<TextView>(R.id.TitleTextView)
        title.text = mynote.title
        val notetext = holder.itemView.findViewById<TextView>(R.id.NoteTextTextView)
        notetext.text = mynote.text
    }

    override fun getItemCount(): Int {
        return notes.size
    }

}