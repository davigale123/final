package com.example.myapplication.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.myapplication.R
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.auth.FirebaseAuth

class NewNoteFragment: Fragment(R.layout.new_note_fragment) {
    private lateinit var exitBtn: Button
    private lateinit var saveBtn:Button
    private lateinit var satauri :EditText
    private lateinit var text : EditText
    private val uid = FirebaseAuth.getInstance().uid
    private val db = FirebaseDatabase.getInstance().getReference("/notes")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        saveBtn = view.findViewById(R.id.saveBtn)
        exitBtn = view.findViewById(R.id.exitBtn)
        satauri = view.findViewById(R.id.satauri)
        text = view.findViewById(R.id.text)

        registerListeners()


    }

    private fun registerListeners(){

        exitBtn.setOnClickListener{
            Navigation.findNavController(requireView())
                .navigate(NewNoteFragmentDirections.actionNewNoteFragmentToHomeFragment())

        }

        saveBtn.setOnClickListener{
            if(saveNoteToDatabase()) {
                saveNoteToDatabase()

                Navigation.findNavController(requireView())
                    .navigate(NewNoteFragmentDirections.actionNewNoteFragmentToHomeFragment())

            }
        }

    }
    private fun saveNoteToDatabase():Boolean{
        val title= satauri.text.toString()
        val text = text.text.toString()
        val note = Note(title, text)
        if (title.isEmpty() || text.isEmpty()){
            Toast.makeText(activity, "title or text is empty", Toast.LENGTH_SHORT).show()
            return false
        }else{
            db.child(uid.toString()).push()
            db.setValue(note)
        }
        return true
    }


}