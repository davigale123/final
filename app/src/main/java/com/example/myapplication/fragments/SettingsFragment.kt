package com.example.myapplication.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth

class SettingsFragment: Fragment(R.layout.settings_fragment) {
        private lateinit var logOutBtn:Button
        private lateinit var passwordChangeBtn:Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        passwordChangeBtn = view.findViewById(R.id.passChangeBtn)
        logOutBtn = view.findViewById(R.id.logOutBtn)
        registerListeners()


    }

    private fun registerListeners() {

        passwordChangeBtn.setOnClickListener(){
            Navigation.findNavController(requireView()).navigate(SettingsFragmentDirections.actionSettingsFragmentToPasswordChangeFragment())
        }

        logOutBtn.setOnClickListener {
            FirebaseAuth.getInstance()
                .signOut()
            Navigation.findNavController(requireView()).navigate(SettingsFragmentDirections.actionSettingsFragmentToAuthorisationFragment())

        }
    }

}