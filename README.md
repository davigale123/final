### How to use:
An amazing Android Application with a well designed XML and it has the facility to add the note whichever the user want. It shows the different background photos on every page whenever the user goes to their profile, settings or home page.
First Screen when the user opens the Notes Application- 


https://ibb.co/7RH0FjW   (Authorisation)



Enter your Email and password and login if you have already registered. Otherwise click on New User! Want To Sign In? button and then enter your working email and set a password of at least length of 9.


https://ibb.co/sCBMbdg   (Registration)




Whenever u sign in the home screen apears, where u can see ur saved notes, also there is a button which allows u to create new note and save it.


https://ibb.co/tmJTQyc   (Home)




User can also recover the password if the user forgot the password by clicking on forgot password? on the login screen. After clicking, user will be directed to below screen. Here, user can enter the registered mail and easily recover the password using Email.


https://ibb.co/pnhydKg   (Password Reset)




If user want to log out from their accounts. they have to go to settings from the navigation bar and there they will press log out, also settings page allows users to change their old passwors into new ones.


https://ibb.co/Wps3zr2  (Password Change)


https://ibb.co/rb2GPM6   (Settings)




In the profile page user can change their photos using "photo url", they can add and change their phone number, address and even name or surname if needed with save button :D.


https://ibb.co/MCPt1rV    (Profile)





We also have faq page where ther is written our slogan "every single note is a new story", there is our phone number in the page below, if you ever need any help just call there.


https://ibb.co/gVrsBCR    (FAQ)



### What is this repository for? ###
-this repository is for final exam.
-V1.0.0
-(https://bitbucket.org/davigale123/final/src/master)

### Used dependeces:
					  implementation 'androidx.core:core-ktx:1.7.0'
    implementation 'androidx.appcompat:appcompat:1.4.0'
    implementation 'com.google.android.material:material:1.4.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.1.2'
    implementation 'com.google.firebase:firebase-auth-ktx:21.0.1'
    implementation 'com.google.firebase:firebase-database:19.2.1'
    implementation 'com.google.firebase:firebase-database-ktx:20.0.3'
    testImplementation 'junit:junit:4.+'
    androidTestImplementation 'androidx.test.ext:junit:1.1.3'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.4.0'

    def nav_version = "2.3.5"
    implementation "androidx.navigation:navigation-fragment-ktx:$nav_version"
    implementation "androidx.navigation:navigation-ui-ktx:$nav_version"

    implementation 'com.github.bumptech.glide:glide:4.12.0'
    annotationProcessor 'com.github.bumptech.glide:compiler:4.12.0'

### Who do I talk to? ###

Owners: Mikheil Doliashvili & Giorgi Kapanadze.
ContactUs:OurFutureSite.com
Number: 551580182